defmodule Examples.FishSims.Std.Fish do
  @moduledoc false
  use Examples.FishSims.Fish

  defp outputs, do: 2
  def ins_and_outs(opts) do
    {ins, _} = Enum.reduce(1..opts.numOfSharks, {[], outputs()+1}, fn _, {ins, num} ->
      {ins ++ [num, num + 1], num + 2}
    end)
    {ins, [1, 2]}
  end

  def step(fish, _living, world) do
    enemies = world.sharks
    fish
      |> stepSim(enemies)
      |> setVector
      |> limitSpeed(fish.opts.fishSpeed)
      |> move
      |> recordAngle
      |> setAlive(enemies)
      |> Map.put(:fitness, fish.fitness + 1)
  end

  def stepSim(fish, enemies) do
    {senses, _} = Enum.reduce enemies, {%{}, outputs()+1}, fn shark, {senses, count} ->
      {
        (senses
          |> Map.put(count,     Utils.closestDif(fish.x, shark.x, fish.opts.size) / fish.opts.size)
          |> Map.put(count + 1, Utils.closestDif(fish.y, shark.y, fish.opts.size) / fish.opts.size)),
        count + 2
      }
    end
    Map.put(fish, :sim, Ann.Simulation.step(fish.sim, senses))
  end
  def setVector(fish) do
    fish
      |> assign(:vec_x, Map.get(fish.sim.data, 1, 0) * fish.opts.fishSpeed)
      |> assign(:vec_y, Map.get(fish.sim.data, 2, 0) * fish.opts.fishSpeed)
  end
  def limitSpeed(fish, speed) do
    factor = 2 #now 0.5 out of 1.0 would hit the max speed.
    vec = {fish.assign.vec_x * factor, fish.assign.vec_y * factor}
    {x, y} = if mag(vec) > speed do
      {x, y} = norm(vec)
      {x * speed, y * speed}
    else
      vec
    end
    fish
      |> assign(:vec_x, x)
      |> assign(:vec_y, y)
  end
  def move(fish) do
    fish
      |> Map.put(:x, Utils.wrap(fish.x + fish.assign.vec_x, fish.opts.size))
      |> Map.put(:y, Utils.wrap(fish.y + fish.assign.vec_y, fish.opts.size))
  end
  def recordAngle(fish) do
    Map.put(fish, :angle, :math.atan2(fish.assign.vec_y, fish.assign.vec_x))
  end
  def setAlive(fish, enemies) do
    Map.put(fish, :alive,
      Enum.all?(enemies, fn shark ->
        :math.sqrt(
          :math.pow(Utils.closestDif(fish.x, shark.x, fish.opts.size), 2) +
          :math.pow(Utils.closestDif(fish.y, shark.y, fish.opts.size), 2)
        ) > fish.opts.sharkRadius
      end)
    )
  end

  def mag({x, y}), do: :math.sqrt(:math.pow(x, 2) + :math.pow(y, 2))
  def norm({x, y}) do
    angle = :math.atan2(y, x)
    {:math.cos(angle), :math.sin(angle)}
  end
  def setMag(vec, mag) do
    {x, y} = norm(vec)
    {x * mag, y * mag}
  end
end
