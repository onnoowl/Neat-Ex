defmodule Examples.FishSims.Std.Sharks do
  @moduledoc false
  def new(opts), do: distribute(Examples.FishSims.Std.Shark, opts)

  def distribute(module, opts) do
    offset = :rand.uniform * 2*:math.pi
    angleMod = 2*:math.pi / opts.numOfSharks
    Enum.map(1..opts.numOfSharks, fn num ->
      angle = (num * angleMod) + offset
      module.new(:math.cos(angle)*opts.sharkDist, :math.sin(angle)*opts.sharkDist, opts)
    end)
  end
end

defmodule Examples.FishSims.Std.Shark do
  @moduledoc false
  use Examples.FishSims.Shark
  alias Examples.FishSims.Std.Fish

  def step(shark, living, _world) do
    shark
      |> setHeading(living)
      |> setVector
      |> limitAngleChange(:math.pi / 20)
      |> Fish.move
  end

  def setHeading(shark, living) do
    closest = Enum.min_by living, fn fish ->
      d = :math.sqrt(
        :math.pow(Utils.closestDif(fish.x, shark.x, shark.opts.size), 2) +
        :math.pow(Utils.closestDif(fish.y, shark.y, shark.opts.size), 2)
      ) / (shark.opts.size/2)
      fishAngle = :math.atan2(Utils.closestDif(fish.y, shark.y, 2 * :math.pi), Utils.closestDif(fish.x, shark.x, 2 * :math.pi))
      a = abs(fishAngle - (shark.angle || fishAngle)) / (:math.pi)
      d*a
    end
    setHeading(shark, closest.x, closest.y)
  end
  def setHeading(shark, x, y) do
    shark
      |> assign(:heading_x, x)
      |> assign(:heading_y, y)
  end

  def setVector(shark) do
    shark
      |> assign(:vec_x, Utils.closestDif(shark.assign.heading_x, shark.x, shark.opts.size))
      |> assign(:vec_y, Utils.closestDif(shark.assign.heading_y, shark.y, shark.opts.size))
  end

  def limitAngleChange(shark, change) do
    curAngle = :math.atan2(shark.assign.vec_y, shark.assign.vec_x)
    oldAngle = shark.angle || curAngle
    angle = if abs(curAngle - oldAngle) > change do
      oldAngle + change * abs(curAngle - oldAngle)/(curAngle - oldAngle)
    else
      curAngle
    end
    shark
      |> assign(:vec_x, :math.cos(angle)*shark.opts.sharkSpeed)
      |> assign(:vec_y, :math.sin(angle)*shark.opts.sharkSpeed)
      |> Map.put(:angle, angle)
  end
end
