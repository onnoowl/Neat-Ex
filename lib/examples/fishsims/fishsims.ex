defmodule Examples.FishSims do
  @moduledoc false
  alias Examples.FishSims.Utils

  def run(mins, annModule \\ Examples.FishSims.Std.Fish, worldModules \\ [Examples.FishSims.Std.Shark], opts \\ Examples.FishSims.Options.new, printEvery \\ -1, fileName \\ nil) do
    if fileName != nil && printEvery > 0, do: File.rm(fileName)

    {inputs, outputs} = annModule.ins_and_outs(opts)
    neat = Neat.new_multi_fitness(
      Ann.new(inputs, outputs, %{}),
      (fn neat ->
        if printEvery >= 1 && rem(neat.generation, printEvery) == 0 do
          fitness_function(neat, annModule, worldModules, opts, fileName, fileName == nil)
        else
          fitness_function(neat, annModule, worldModules, opts)
        end
      end),
      population_size: 150,
      target_species_number: 25
    )

    neat = Neat.evolveForMins neat, mins, fn neat ->
      {_best, fitness} = neat.best
      IO.puts "Generation: #{neat.generation}, best fitness: #{fitness}"
    end

    {_ann, fitness} = neat.best
    IO.puts "Best fitness: #{fitness}"
    neat
  end

  def fitness_function(neat, annModule, worldModules, opts, fileName \\ nil, ascii \\ false) do
    species = Enum.map neat.species, fn {rep, members} ->
      {rep, Enum.map(members, fn ann -> annModule.new(ann, opts) end)}
    end
    world = initWorld(worldModules, opts)

    {:ok, file} = if fileName != nil, do: File.open(fileName, [:append]), else: {:ok, nil}
    species = simulate(species, world, neat.generation, opts, file, ascii)
    if fileName != nil, do: (:ok = File.close file)

    count = length(Utils.flatten(species))

    species = Neat.Utils.async_map species, fn simAnn ->
      {
        simAnn.ann,
        single_sim(annModule.new(simAnn.ann, opts), world, opts),
        simAnn.fitness
      }
    end
    {eS, eM} = Neat.Utils.reduce species, {0, 0}, fn {_, s, m}, {eS, eM} -> #estimated values (average)
      {eS + s/count, eM + m/count}
    end
    {varS, varM} = Neat.Utils.reduce species, {0, 0}, fn {_, s, m}, {varS, varM} -> #average variance
      {varS + :math.pow(s - eS, 2)/count, varM + :math.pow(m - eM, 2)/count}
    end
    {sdS, sdM} = {:math.sqrt(varS), :math.sqrt(varM)} #standard deviation

    maxMulti = Enum.flat_map(species, fn {_, members} -> Enum.map(members, fn {_, _, m} -> m end) end) |> Enum.max
    species = Neat.Utils.map species, fn {ann, s, m} ->
      {zS, zM} = {(s - eS)/sdS, (m - eM)/sdM} #converted z values
      {ann, zS + zM}
    end

    {minZ, maxZ} = Enum.flat_map(species, fn {_, members} -> Enum.map(members, fn {_, z} -> z end) end) |> Enum.min_max

    Neat.Utils.map species, fn {ann, z} ->
      {ann, maxMulti * ((z - minZ) / (maxZ - minZ))}
    end
  end

  def single_sim(fish, world, opts) do
    [{_, [simAnn]}] = simulate([{nil, [fish]}], world, nil, opts, nil, nil)
    simAnn.fitness
  end

  def simulate(species, world, generation, opts, file, asciiArt) do
    living = species |> Utils.flatten |> Enum.filter(fn fish -> fish.alive end)
    if length(living) > 0 do
      visuals(species, world, generation, opts, file, asciiArt)
      simulate(
        Neat.Utils.async_species_map(species, fn {rep, members} ->
          {rep, Enum.map(members, fn simAnn ->
            simAnn.__struct__.step(simAnn, living, world)
          end)}
        end),
        stepWorld(world, [living, world]),
        generation,
        opts,
        file,
        asciiArt
      )
    else
      species
    end
  end

  def initWorld(m = %{}, opts) do
    Enum.reduce m, %{}, fn {k, v}, acc ->
      Map.put(acc, k, initWorld(v, opts))
    end
  end
  def initWorld([h | t], opts), do: [initWorld(h, opts) | initWorld(t, opts)]
  def initWorld([], _opts), do: []
  def initWorld(module, opts) when is_atom(module), do: module.new(opts)
  def initWorld(obj, _opts), do: obj

  def stepWorld(m = %{}, args) do
    if Map.has_key?(m, :__struct__) do
      apply(&m.__struct__.step/3, [m | args])
    else
      Enum.reduce m, %{}, fn {k, v}, acc ->
        Map.put(acc, k, stepWorld(v, args))
      end
    end
  end
  def stepWorld([h | t], args), do: [stepWorld(h, args) | stepWorld(t, args)]
  def stepWorld([], _args), do: []

  def visuals(species, world, generation, opts, file, asciiArt) do
    if asciiArt do
      :timer.sleep(20)
      IO.puts "\n#{to_s(species, world, generation, opts)}"
    end
    if file != nil, do: IO.puts(file, visualJson(species, world, generation, opts))
  end

  def to_s(species, world, generation, opts, w \\ 79, h \\ 22) do
    size = opts.size
    fishies = species |> Utils.flatten |> Enum.filter(fn fish -> fish.alive end)
    fishCounts = Enum.reduce fishies, %{}, fn fish, acc ->
      Map.update(acc, {scale(fish.x, size, w), scale(fish.y, size, h)}, 1, &(&1 + 1))
    end
    max = if Map.size(fishCounts) > 0, do: Enum.max(Map.values(fishCounts)), else: 1
    displayHash = Enum.reduce fishCounts, %{}, fn {pos, val}, newHash ->
      Map.put(newHash, pos, trunc((val / max) * 9))
    end
    displayHash = Enum.reduce world, displayHash, fn {_, obj}, displayHash -> char(obj, displayHash, size, w, h) end

    (Enum.map_join 0..(h-1), "\n", fn y ->
      Enum.map_join 0..(w-1), "", fn x ->
        pos = {x, y}
        Map.get(displayHash, pos, " ")
      end
    end) <> "\nGeneration: #{generation}, fish left: #{length fishies}"
  end
  def char([obj | t], displayHash, size, w, h), do: char(t, char(obj, displayHash, size, w, h), size, w, h)
  def char([], displayHash, _, _, _), do: displayHash
  def char(obj, displayHash, size, w, h) do
    Map.put(displayHash, {scale(obj.x, size, w), scale(obj.y, size, h)}, obj.__struct__.char(obj))
  end
  def scale(val, size, newMax), do: trunc(((val + size/2) / size) * newMax)

  def visualJson(species, world, generation, _opts) do
    {:ok, json} = JSON.encode(
      Map.new
        |> Map.put("generation", generation)
        |> Map.put("fish", (Utils.flatten(species)
              |> Enum.filter(fn fish -> fish.alive end)
              |> mapify
            )
          )
        |> (fn map ->
            Enum.reduce(world, map, fn {key, obj}, map ->
              Map.put(map, Atom.to_string(key), mapify(obj))
            end)
          end).()
    )
    json
  end
  defp mapify([obj | t]), do: [mapify(obj) | mapify(t)]
  defp mapify([]), do: []
  defp mapify(obj), do: obj.__struct__.mapify(obj)
end
