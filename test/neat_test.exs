defmodule NeatTest do
  use ExUnit.Case

  setup do
    neat = Map.put(Neat.new_single_fitness(Ann.new([1, 2, 3], [4], %{}), fn _ -> :rand.uniform end, population_size: 15, target_species_number: 3), :conn_id, 9)

    {:ok, [neat: neat]}
  end

  test "Neat.evolve", context do
    neat = Enum.reduce 1..10, context[:neat], fn _, neat -> Neat.evolve(neat) end
    assert neat #makes sure it runs and is not nil
  end

  # test "Neat.speciate", context do
    # thresh = 1.0
    # neat = context[:neat]
    #   |> Map.put(:opts, Map.put(context[:neat].opts, :compatibility_threshold, thresh))
    #   |> Neat.evolve #First use of evolve generates a population, then speciates it.

    # Enum.each neat.species, fn {rep, members} ->
      # IO.puts "Rep: #{inspect(rep)}"
      # Enum.each(members, fn ann -> IO.puts("  #{inspect(ann)}") end)
      # Enum.each(members, fn ann -> assert(Neat.compatibility(neat.opts, rep, ann) <= thresh) end)
    # end
  # end
end
