# Notes

This is a collection of (fairly unorganized) design notes.

## NEAT Main

This is a description of the main looping, high level function, that runs during a NEAT evolution.

If population is empty, generate the members of the population with mutated versions of the seed network.
If the number of connections in the seed network equals zero, use new_link to mutate offspring. Otherwise, include the seed network in the population, and use normal mutation. Speciate the new members with the speciation function.

If in single_fitness mode:
    [species1, species2, species3]
    |> Stream.map(fn(species) -> Task.async(fn() -> Enum.map(species, fn(network) -> eval_fitness(network) end) end) end)
    |> Enum.map(&Task.await/1)

If in multi_fitness mode, send the fitness function the entire species list.

Iterate through each species, and assign an average fitness of its members.

For each species, kill all but the best (:survival_threshold ratio, like 20%) of the members. Round up to the nearest number for how many to save.

In each species, randomly reproduce with the top tier members, `(AverageSpeciesFitness / Total_of_AverageSpeciesFitnesses) * PopulationSize - NumberOfTopTierMembers` times.
Integrate new offspring using the speciation function.

If species number is greater than ideal, combine the two closest species, then increase compatibility threshold. Otherwise, decrease compatibility threshold.


Speciation function: (takes a list of tuples, with it's parent species by index, and the network) For each new member, check if it's compatible with the parent species. If not, iterate through existing species checking for compatibility. If not compatible, make a new species.

## ANN Evaluation

Each node adds up the activation from all incoming nodes from the *previous timestep*. A network will take several timesteps until the output is evaluated.

For problems that take an instantaneous solution, and don't expect for the network to be run over time, the network would need to be run for several time ticks until the output stabilizes.

### ANN Evaluation Algorithm:

1. Create `current`. This is a lightweight data structure (like a hash) for storing the values for each node. `current` contains all the nodes of the network, only their `output` values are all 0.
2. Create list `evaluating`. This is a list of nodes to be evaluated. When created, it is filled with the input nodes. **Note:** `evaluating` should only contain references to node data as they exist inside `current` (or `previous`, as introduced in step 3)
3. Copy `current` into variable `previous`.
4. Create list `evaluating_next`. This is a list of neurons to be evaluated in the next frame. Starts empty.
5. For each `node` in `evaluating` do:
    6. Evaluate output:
        - If `node` is an input
            - Fetch the corresponding input value, save it as `output`.
        - otherwise
            - Create variable `val`, with value 0.
            - For each `in_node` connected to `node` do:
                - Get it's output from the data structure `previous`
                - Multiply it by the connection weight
                - Then and add it to `val`.
            - Variable `output` is created with the value `sigmoid_function(val)`.
    7. For every node that uses the output of `node`, add them to `evaluating_next`.
    8. Set the `output` of the `node`, as it exists in `current`.
9. Copy `evaluating_next` into `evaluating`.
10. Repeat steps 3-10 until end of simulation, or until output data stabalizes (when `previous` equals `current`).

## Runtime parameters

These are the standard parameters specified on the website.
The paramater that **most likely needs to be changed** if the system isn't working is the **MutationPower**. The other two to consider are the parameters **TargetSpeciesNumber** and **PopulationSize**.

    difference_coefficient 2.0
    weight_difference_coefficient 1.0

These are the standard NEAT compatibility coefficients for the compatibility equation the determines how far apart two individuals are for the purposes of speciation. In general, the question is how many gene differences are worth an average distance of 1.0 in weights. The numbers above say that an average difference of 1.0 in weights is like having two genes not shared between the two individuals. That is pretty reasonable.

    compatibility_threshold 6.0

This parameter relates to the coefficients above. It can be thought of as, how many genes not shared does it take for individuals to be considered in a different species. However, it is only relevant at the start because the next parameter lets it change.

    compatibility_modifier 0.3

This number means the threshold changes at a rate of 0.3 per generation, which is reasonable. It does not change if the SpeciesSize Target is already met.

    dropoff_age 15.0

In 15 generations a species will be penalized if it is not making progress.

    survival_threshold 0.2

Only the top 20% of each species is allowed to reproduce. Controls greediness within species.

    weight_mutation_power 2.5

Mutations of weight go up to 2.5 in a single mutation. You wouldn't want it over 5.0 or so.

    target_species_number *?*

The CompatibilityThreshold will be automatically modified to meet this species number target target. *I think a number between 10-15 would be a good value?*

    population_size 150

The PopulationSize constraint is always approximately met. The population size will consistently be close to it, but two to truncation, it may be a little less or more than the target. *Could resonably be any value from 150-1000, perhaps even higher.*

## Automatic threshold settings

The threshold should be automatically modified. If the number of species is less than the TargetSpeciesNumber, then the CompatibilityThreshold is decreased by the CompatibilityModifier. If the number of species is greater than the TargetSpeciesNumber, it's increased instead.
If the CompatibilityThreshold becomes less than the CompatibilityModifier, then it is set to be equal to the CompatibilityModifier.

## Fitness computation

Fitness values must be **positive only**. No negative fitnesses are allowed.

## Speciation optimization

When a child network is created, store the species of the parent. When checking to see what species the child belongs to, check the species of the parent first. If that fails, then run through the line of all species. Since children usually belong to the species of their parents, this dramatically improves performance.

## Inovation record keeping

> **Undecided:** Should a record of existing innovations be kept just for the current generation, or forever?

## Number of offspring

The number of members of a species in the next generation should be equal to `(AverageSpeciesFitness / Total_of_AverageSpeciesFitnesses) * PopulationSize`

## Reproduction

There's a 75% chance that a child is created from mating, and a 25% chance that it is a result of mutation. 0.1% of the population is a result of interspecies mating.

### Mutation probabilities

> Based on these probabilities, I suppose for a small population, there is an 18.43% chance that the mutated child is identical to the parent- which I suppose is fine... For a large population, the chance is only 7%.

Small population (150 networks):
- New node: 3%
- New link: 5%
Large population (1000 networks):
- New node: 30%
- New link: 50%

> **Should** these be paramaters? Or should they be decided automatically, based on the population size? (We have 2 sample data points, so we can plot any point on the line given a population size.)

80% chance of having mutation weights mutated.
Each connection weight has a 90% chance of randomly perturbed, and a 10% chance of receiving a random new value.

> **Not sure** how mutation power is used...
> My best guess is to use it as the standard deviation for the "random perturbation", meaning when a weight is mutated, a random number is generated from a weighted distribution of standard deviation <MutationPower>, which is then added to the existing weight. This results in the weight usually being changed by small amounts.

Random weights should be randomly generated from a normal distribution with a standard deviation of 1.

## Sigmoid function

    f(x) => tanh(2 * x)
