defmodule Neat.Mixfile do
  use Mix.Project

  def project do
    [
      app: :neat_ex,
      version: "1.3.0",
      elixir: "~> 1.4",
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      name: "Neat-Ex",
      source_url: "https://gitlab.com/onnoowl/Neat-Ex",
      docs: [main: Neat]
    ]
  end

  def application do
    # Specify extra applications you'll use from Erlang/Elixir
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      { :json,   "~> 0.3.0"},
      {:ex_doc, "~> 0.14", only: :dev, runtime: false},
      {:siphash, "~> 3.1.1"}
    ]
  end

  defp description do
    """
    This project provides the means to define, simulate, and serialize Artificial-Neural-Networks (ANNs), as well as the means to develop them through use of the Neuro-Evolution of Augmenting Toplogies (NEAT) algorithm created by Dr. Kenneth Stanley.
    """
  end

  defp package do
    [
      maintainers: ["Stuart Hunt"],
      licenses: ["MIT License"],
      links: %{"GitLab" => "https://gitlab.com/onnoowl/Neat-Ex"}
    ]
  end
end
